#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("EEkIGhocBAwaSQgKCgwZHQgHCgwFDEkgBwpHWE9ZTW9qPG1ienQoGXb4sncuOYJshDcQ7USCX8s+JTyF/PcTZc0u4jK9f15aoq1mJKd9ALhsaWrraGZpWetoY2vraGhpjfjAYH9ZfW9qPG1qemQoGRkFDEk7BgYdRlnoqm9hQm9obGxua2tZ6N9z6NrraGlvYEPvIe+eCg1saFnom1lDbxNZ62gfWWdvajx0Zmholm1tamto150a8oe7DWaiECZdsctXkBGWAqFhQm9obGxua2h/dwEdHRkaU0ZGHrBfFqjuPLDO8NBbK5KxvBj3F8g7YTdZ62h4b2o8dElt62hhWetobVlcW1hdWVpfM35kWlxZW1lQW1hdWcG1F0tco0y8sGa/Ar3LTUp4nsjFbW96azw6WHpZeG9qPG1jemMoGRnicOC3kCIFnG7CS1lrgXFXkTlguh0ADwAKCB0MSQsQSQgHEEkZCBsdVE8OSeNaA55k66a3gspGkDoDMg0sF3YlAjn/KOCtHQtieeoo7lrj6B4eRwgZGQUMRwoGBEYIGRkFDAoIwsoY+y46PKjGRijakZKKGaSPyiXmGugJr3IyYEb725EtIZkJUfd8nE9ZTW9qPG1ienQoGRkFDEkqDBsd3FPEnWZnafti2Eh/Rx28VWSyC39ZeG9qPG1jemMoGRkFDEkgBwpHWGRvYEPvIe+eZGhobGxpautoaGk1b2o8dGdtf219QrkALv0fYJedAuQwzmxgFX4pP3h3Hbre4kpSLsq8BiCxH/ZafQzIHv2gRGtqaGloyutob1lmb2o8dHpoaJZtbFlqaGiWWXQLBQxJGh0IBw0IGw1JHQwbBBpJCKkKWh6eU25FP4KzZkhns9MacCbcDuZh3UmeosVFSQYZ31ZoWeXeKqYbCAodAAoMSRodCB0MBAwHHRpHWQ1cSnwifDB02v2en/X3pjnTqDE5FijB8ZC4ow/1TQJ4ucrSjXJDqnbectT6K017Q65mdN8k9TcKoSLpfgcNSQoGBw0AHQAGBxpJBg9JHBoMAA8ACggdAAYHSSgcHQEGGwAdEFhD7yHvnmRoaGxsaVkLWGJZYG9qPFnrbdJZ62rKyWpraGtraGtZZG9gOwwFAAgHCgxJBgdJHQEAGkkKDBtJCAcNSQoMGx0ADwAKCB0ABgdJGaBwG5w0Z7wWNvKbTGrTPOYkNGSYTYuCuN4ZtmYsiE6jmAQRhI7cfn5aXzNZC1hiWWBvajxtb3prPDpYehkFDEk7BgYdSSooWXd+ZFlfWV1bZvRUmkIgQXOhl6fc0GewN3W/olRuhRRQ6uI6SbpRrdjW8yZjApZClUkqKFnraEtZZG9gQ+8h755kaGhoRynPni4kFmE3WXZvajx0Sm1xWX/YWTGFM21b5QHa5nS3DBqWDjcM1Xbs6uxy8FQuXpvA8innRb3Y+XuxSQYPSR0BDEkdAQwHSQgZGQUACggZBQxJKgwbHQAPAAoIHQAGB0koHF/wJUQR3oTl8rWaHvKbH7seWSao6X1CuQAu/R9gl50C5Ecpz54uJBZFSQoMGx0ADwAKCB0MSRkGBQAKEB0BBhsAHRBYf1l9b2o8bWp6ZCgZOcPjvLONlblgbl7ZHBxI");
        private static int[] order = new int[] { 40,3,15,18,14,12,37,34,28,50,35,58,33,34,54,15,16,39,56,25,36,24,55,35,35,50,47,30,52,30,30,42,57,35,46,36,39,59,43,58,56,56,43,45,50,53,52,59,55,51,52,53,59,53,56,56,56,57,59,59,60 };
        private static int key = 105;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
