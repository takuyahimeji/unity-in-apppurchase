#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("RpCgdOg4CPhF0T53d14/ZEakFiN9Q3sT9U5d/YvFumch0rMIFHpo0etUQQsCNcVbT6eu5YWdZuMeJYZBjwjGhcdwa/U8/dIlHBHPfbWtzgjX2A4p9jU5PzjXhyXA/0vBdYZrTcLOff3nX7hIMxtrsQZOOUBsSvAAR+EU4nn3ixWvInxrKHx9WGjzzMn8zvNTgTBmhYprvpyRn1VDHnmTxMom212JM8eC3eCWjpGOE6z/h/ahTpDvAki3jK8b2vIlm19CXQZXOIUqqaeomCqpoqoqqamoMCYGlMWsnJgqqYqYpa6hgi7gLl+lqamprairyc8aPtiFpywr/3xTOUcS03Ve3wEowzP4GCx4J8VGMmNzONpJrMGFWtXs9VS2tYu0jaqrqaip");
        private static int[] order = new int[] { 13,6,9,5,9,11,8,12,12,11,12,13,12,13,14 };
        private static int key = 168;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
